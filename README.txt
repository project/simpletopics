Copyright 2012 - Fabrizio Ranieri

Module Description:
-------------------
The module provides integration between Drupal and SMF Forum. Each time a node 
is being inserted, a topic is created in the SMF Forum. Mapping between Drupal 
and SMF is provided by smfforum module (required), while the mapping between 
content types and SMF boards is allowed in configuration pages.

Features:
---------
@Todo.

Requirements:
-------------
This is a Drupal 6 module. smfforum module is also required.

Installation:
-------------
See INSTALL.txt

Bugs/Features/Patches:
----------------------
@Todo.
