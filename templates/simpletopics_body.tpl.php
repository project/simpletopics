<?php
/**
 * @file
 * Template for simpletopics_node_body theme. This should render the node body
 * for replication into SMF forum
 */

?> 
[b]<?php print $node->title?>[/b]
[quote]<?php print $node->body?>[/quote]
<?php print t('Back to the node:') . l($node->title, "node/{$node->nid}", array('absolute' => TRUE))?>
