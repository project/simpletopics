<?php
/**
 * @file
 * Administration inc. source
 */

/**
 * Main settings form.
 * @todo flag: per-board delete topic on node delete
 * @todo flag: per-board update topic on node update
 * @return form
 *    The settings form
 */
function simpletopics_settings_form() {
  $form = array();
  $types = node_get_types();
  $boards = simpletopics_get_boards();
  $board_mapping = simpletopics_get_board_mapping();
  $member = simpletopics_get_member();

  $form["general"] = array(
    '#type' => 'fieldset',
    '#title' => t('General configuration'),
    '#description' => t(''),
    '#collapsible' => FALSE,
    '#tree' => FALSE,
  );

  $form['general']['member'] = array(
    '#type' => 'textfield',
    '#title' => t('Member name'),
    '#required' => TRUE,
    '#description' => t('Set the Member Name you want to use as the topic author. This should be a valid SMF user.'),
    '#default_value' => !empty($member) ? $member['memberName'] : '',
  );

  if (!empty($member)) {
    foreach ($types as $type) {
      $form["types_{$type->type}"] = array(
        '#type' => 'fieldset',
        '#title' => t("Content type @type", array('@type' => $type->name)),
        '#description' => t('Here you can map the @type nodes replication with SMF available boards.', array('@type' => $type->name)),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => FALSE,
      );
      foreach ($boards as $board) {
        $form["types_{$type->type}"]["simpletopics_mapping:{$type->type}:$board->ID_BOARD"] = array(
          '#type' => 'checkbox',
          '#default_value' => !empty($board_mapping[$type->type]) ? in_array($board->ID_BOARD, $board_mapping[$type->type]) ? 1 : 0 : 0,
          '#title' => t("Enable @board board", array('@board' => $board->name)),
          '#description' => t(''),
        );
      }
    }
  }
  else {
    drupal_set_message(t('In order access full configuration you should provide a valid SMF username.'), 'warning');
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Handler for settings_form submit.
 */
function simpletopics_settings_form_submit($form, &$form_state) {
  $result = array();
  foreach ($form_state['values'] as $key => $value) {
    $chunks = explode(':', $key);
    if (count($chunks) != 3 || !$value) {
      continue;
    }
    $result[$chunks[1]][] = $chunks[2];
  }
  if (!empty($result)) {
    simpletopics_save_board_mapping($result);
  }
  $member = $form_state['values']['member'];
  simpletopics_save_member($member);
  drupal_set_message(t('Changes have been saved'));
}

/**
 * Handler for settings_form validate.
 */
function simpletopics_settings_form_validate($form, &$form_state) {
  $member = $form_state['values']['member'];
  if (!empty($member)) {
    if (_simpletopics_fetch_user($member) == NULL) {
      form_set_error('member_id', t('The requested Member Name cannot be found.'));
    }
  }
}
